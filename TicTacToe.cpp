#include "TicTacToe.h"

//creating the gridstructure with for loops 
//and giving it an interger character for each
//row and collum [x] [y]
// 1 2 3
// 4 5 6
// 7 8 9

void TicTacToe::grid()
{
	
	int num = 1;
	for (int x = 0; x < GRID_SIZE; x++)
	{
		for (int y = 0; y < GRID_SIZE; y++)
		{
			Grid[x][y] = to_string(num).c_str()[0];
			num += 1;
			cout << "\t" << endl;
		}
	}
}
//Now that we have the structure of the grid with the intergers
//We can repeat the same thing except we draw 
//lines to make a tic tac toe grid look nicer :)
//1 | 2 | 3 |
//------------
//4 | 5 | 6 |
//------------
//7 | 8 | 9 |
//------------
void TicTacToe::drawGrid()
{
	system("cls"); //to clear previous grid and update new one
	cout << "=============\n";
	cout << "-TIC TAC TOE-" << endl;
	printf("------------\n");
	for (int x = 0; x < GRID_SIZE; x++)
	{
		
		for (int y = 0; y < GRID_SIZE; y++)
		{

			printf(" %c |", Grid[x][y]);
			 

		}
		printf("\n------------\n");
	}

	cout << "\n(M)MAIN MENU\n";
	cout << "\nP1: X P2: O\n";
	cout << "=============\n";


}

//This function is excecuted after 
//every turn to check for wins
bool TicTacToe::checkWin()
{
	string playAgain;
	const char* winning_moves[8] = 
	//possible win patterns stored in a const char*
	{
		"123",
		"456",
		"789",
		"147",
		"258",
		"369",
		"159",
		"753"
	};
	//loop through the possible win patterns
		for (int i = 0; i < 8; i++)
		{
			bool win = true; //win set to true
			char previous_grid = ' ';
			const char* winning_move = winning_moves[i];

			//loop through every row and collum
			//of the previous grid to search for
			//a win pattern
			for (int index = 0; index < GRID_SIZE; index++)
			{
				//index variable used to replace i
				char character = winning_move[index];
				//GRID_SIZE = 3
				int entered_number = character - '0';
				int grid_space = entered_number - 1;
				//entered numbers and character
				int row = grid_space / GRID_SIZE;
				int col = grid_space % GRID_SIZE;
				
				char grid_char = Grid[row][col];
				//^looping through the previous grid
				//for the size of the GRID

			    //if no wins, break outta the loop 
				//and the game goes on
				if (previous_grid == ' ')
				{
					previous_grid = grid_char;
				}
				else if (previous_grid == grid_char)
				{
					continue;
				}
				else
				{
					win = false; 
					break;
				}
			}
			if (win)
			{
			//if win pattern detected
			//Game ends and the player
		    //will be sent back to the menu
				cout << "     ___________\n";
				cout << "    '._==_==_=_.'\n";
				cout << "    .-\:      /-.\n";
				cout << "   | (|:.     |) |\n";
				cout << "    '-|:.     |-'\n";
				cout << "      \::.    /\n";
				cout << "       '::. .'\n";
				cout << "        ) (\n";
				cout << "      _.' '._\n";
				cout << "     `--------'\n";
				//trophy to congratulate winner :3
				cout << "\-WE HAVE A WINNER-\n";
				printf("\n-%c HAS WON THE GAME!-\n", previous_grid);
				cout << "\n-Press 'Enter' to go back :)-";
				getline(cin, playAgain);

				if (playAgain != " ")
				{

					system("cls");
					mainMenu(); //clear grid and return to main menu
					break;
				}
			}
		}
	return(0);
}


void TicTacToe::player1()
{
	    //player 1 
		string input1;	

		while (true)
		{
			
			cout << "Player 1, you are X, select a spot\n";
			getline(cin, input1);
			
			
			if (input1 != " ")
			{
				//player input
				char entered = input1.c_str()[0];
				//if player input is 1-9 
				//loop through grid
				if (entered >= '1' && entered <= '9')
				{
					int entered_number = entered - '0';
					int index = entered_number - 1;

					int row = index / GRID_SIZE;
					int col = index % GRID_SIZE;

					char grid_position = Grid[row][col];
					//if player inputs in a filled row & collum
					//game will keep warning till player finds an 
					//empty row & collum
					if (grid_position == 'X' || grid_position == '0')
					{
						cout << "That postiton is already taken ...\n";
					}
					else
					{
						Grid[row][col] = 'X';
						break;
					}
				}
				else
				{
					cout << "Enter an empty cell (1-9)\n";
			    }
			}
			//if M is the user input it sends player to menu
			//restarting or aborting game
			if (input1 != " ")
			{
				char entered = input1.c_str()[0];

				if (entered == 'M' || entered == 'm')
				{
					system("cls");
					mainMenu();
				}

				
			}
			

		}
		
}


void TicTacToe::player2()
{
	//player 2
	string input2;

	while (true)
	{
		//function is the same with player 1
		cout << "Player 2, you are 0, select a spot\n";
		getline(cin, input2);
		
		if (input2 != " ")
		{
			char entered = input2.c_str()[0];

			if (entered >= '1' && entered <= '9')
			{
				int entered_number = entered - '0';
				int index = entered_number - 1;

				int row = index / GRID_SIZE;
				int col = index % GRID_SIZE;

				char grid_position = Grid[row][col];

				if (grid_position == 'X' || grid_position == '0')
				{
					cout << "That postiton is already taken \n";
				}
				else
				{
					Grid[row][col] = '0';
					break;
				}
			}
			else
			{
				cout << "Enter an empty cell (1-9)\n";
			}
		}
		//player can abort game by entering 'm'
		if (input2 != " ")
		{
			char entered = input2.c_str()[0];

			if (entered == 'M' || entered == 'm')
			{
				system("cls");
				mainMenu();
			}

		}

	}

}

void TicTacToe::computer()
{
	
	srand(time(0));
	while (true)
	{
		//computer randomly selects empty space
		int ai_choice = ( rand() % 9) + 1;
		//computer searching through grid
		int row = (ai_choice - 1) / 3;
		int col = (ai_choice - 1) % 3;
		
		char grid_postion = Grid[row][col];
		//if computer selected filled spot 
		//computer loop repeats till true
		if (grid_postion == 'X' || grid_postion == '0')
		{
			continue;
		}
		else
		{
			Grid[row][col] = '0';
			break;
		}
	}
}


//game modes
//looping through functions
//till game ends
void TicTacToe::multiPlayer()
{
	//construct grid
	grid();
	drawGrid();
	while (true)
	{
		//player 1 turn
		checkWin();
		drawGrid();
		player1();
		drawGrid();

		//player 2 turn
		checkWin();
		drawGrid();
		player2();
		drawGrid();
	}
}


void TicTacToe::singlePlayer()
{
	//construct grid
	grid();
	drawGrid();
	
	while (true)
	{
		//check wins and update grid
		//every turn
		checkWin();
		drawGrid();
		player1();
		drawGrid();

		//computer turns
		checkWin();
		drawGrid();
		computer();
		drawGrid();
	}
}

void TicTacToe::mainMenu()
{
	//main menu
	int mode;
	cout << "========================";
	cout << "\n-T I C  T A C  T O E- \n\n";
	cout << "(1) SINGLEPLAYER\n";
	cout << "     ^you vs computer!\n\n";
	cout << "(2) MULTIPLAYER\n";
	cout << "     ^you vs a friend!\n\n";
	cout << "(3) EXIT\n";
	cout << "========================\n";

	cin >> mode;
	//based on players input either of the 
	//functions will excecute
	switch (mode) {
	case 1:
		singlePlayer();
		break;
	case 2:
		multiPlayer();
		break;
	case 3:
		exit(0);
		break;
	default:
		cout << "Please enter a correct key \n";
		break;
		//if player inputs an incorrect key game warns
	}
	
}







