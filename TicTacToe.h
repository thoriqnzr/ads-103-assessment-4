#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <chrono>
#include <windows.h>
#include <thread>
#define GRID_SIZE 3 //grid size IMPORTANT

using namespace std;
//using classess to make things easier
class TicTacToe
{
public:
	//the tic tac toe grid
	void grid();
	void drawGrid();
	//game states
	bool checkWin();
	void player1();
	void player2();
	void computer();

	
	//gamemodes
	void multiPlayer();
	void singlePlayer();
	void mainMenu();
	
	char Grid[GRID_SIZE][GRID_SIZE]; //defining the grid x y
	
};

